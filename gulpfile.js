"use strict";

const browsersync = require("browser-sync").create();
const del = require("del");
const gulp = require("gulp");
const plumber = require("gulp-plumber");
const concat = require('gulp-concat');
const rename = require("gulp-rename");
const sass = require("gulp-sass");
const autoPrefixer = require("gulp-autoprefixer");
const minifyCSS = require("gulp-minify-css");
const minifyJS = require("gulp-minify");

const env = {
    server: {
        path: './',
        port: 3000,
    },
    paths: {
        sass: {
            src: [
                './src/scss/**/*.scss'
            ],
            dest: './public/css/'
        },
        js: {
            src: './src/js/**/*.js',
            dest: './public/js/'
        },
        jsLibraries: {
            jquery: {
                src: './node_modules/jquery/dist/jquery.min.js'
            }
        }
    }
}

// BrowserSync
function browserSync(done) {
    browsersync.init({
        server: {
            baseDir: env.server.path,
        },
        port: env.server.port
    });

    done();
}

// BrowserSync Reload
function browserSyncReload(done) {
    browsersync.reload();
    done();
}

// Clean assets
function clean() {
    return del(["./public/"]);
}

// CSS task
function css() {
    return gulp
        .src(env.paths.sass.src)
        .pipe(plumber())
        .pipe(sass({ outputStyle: "expanded" }))
        .pipe(gulp.dest(env.paths.sass.dest))
        .pipe(rename({ suffix: ".min" }))
        .pipe(autoPrefixer())
        .pipe(minifyCSS())
        .pipe(gulp.dest(env.paths.sass.dest))
        .pipe(browsersync.stream());
}

// Transpile, concatenate and minify scripts
function scripts() {
    return (
        gulp
            .src([
                env.paths.jsLibraries.jquery.src,
                env.paths.js.src
            ])
            .pipe(concat('main.js'))
            .pipe(minifyJS())
            .pipe(gulp.dest(env.paths.js.dest))
            .pipe(browsersync.stream())
    );
}

// Watch files
function watchFiles() {
    gulp.watch(env.paths.sass.src, css);
    gulp.watch(env.paths.js.src, gulp.series(scripts));
    gulp.watch(["./*"], gulp.series(browserSyncReload));
}

// define complex tasks
const js = gulp.series(scripts);
const build = gulp.series(clean, gulp.parallel(css, js));
const watch = gulp.parallel(watchFiles, browserSync);

// export tasks
exports.css = css;
exports.js = js;
exports.clean = clean;
exports.build = build;
exports.watch = watch;
exports.default = build;