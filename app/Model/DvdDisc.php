<?php
/**
 * Class DvdDisc
 *
 * @package App\model
 */
class DvdDisc extends BaseProduct
{
    /** @var int  */
    protected int $size;

    /**
     * DvdDisc constructor.
     *
     * @param array $productParams
     */
    public function __construct(array $productParams)
    {
        parent::__construct(
            $productParams['id'],
            $productParams['sku'],
            $productParams['name'],
            $productParams['price'],
            $productParams['type']
        );

        $this->size = $productParams['size'];
    }

    /**
     * @param int $size
     */
    public function setSize(int $size): void
    {
        $this->size = $size;
    }

    /**
     * @return int
     */
    public function getSize(): int
    {
        return $this->size;
    }
}