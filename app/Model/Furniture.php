<?php
/**
 * Class Furniture
 */
class Furniture extends BaseProduct
{
    /** @var string  */
    protected string $dimension;

    /**
     * Furniture constructor.
     *
     * @param array $productParams
     */
    public function __construct(array $productParams)
    {
        parent::__construct(
            $productParams['id'],
            $productParams['sku'],
            $productParams['name'],
            $productParams['price'],
            $productParams['type']
        );

        $this->dimension = $productParams['dimensions'];
    }

    /**
     * @return string
     */
    public function getDimension(): string
    {
        return $this->dimension;
    }

    /**
     * @param string $dimension
     */
    public function setDimension(string $dimension): void
    {
        $this->dimension = $dimension;
    }
}