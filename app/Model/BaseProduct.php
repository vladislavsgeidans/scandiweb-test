<?php
/**
 * Abstract class BaseProduct
 *
 * @package App\Model
 */
abstract class BaseProduct
{
    /** @var int  */
    protected int $id;

    /** @var string  */
    protected string $sku;

    /** @var string  */
    protected string $name;

    /** @var float  */
    protected float $price;

    /** @var string  */
    protected string $type;

    /**
     * BaseProduct constructor.
     *
     * @param string $id
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $type
     */
    protected function __construct(
        string $id,
        string $sku,
        string $name,
        float $price,
        string $type
    )
    {
        $this->id = $id;
        $this->sku = $sku;
        $this->name = $name;
        $this->price = $price;
        $this->type = $type;
    }

    /**
     * @param string $sku
     * @param string $name
     * @param float $price
     * @param string $type
     * @param int|null $size
     * @param float|null $weight
     * @param string|null $dimensions
     * @return bool
     */
    static function save(string $sku, string $name, float $price, string $type, int $size = null, float $weight = null, string $dimensions = null): bool
    {
        $result = mysqli_query(Database::getInstance(),
            "INSERT INTO products (sku, name, price, type, size, weight, dimensions)
                    VALUES ('$sku', '$name', $price, '$type', $size, $weight, '$dimensions')");

        if ($result) {
            return true;
        }

        return false;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }

    /**
     * @return string
     */
    public function getSku(): string
    {
        return $this->sku;
    }

    /**
     * @param string $sku
     */
    public function setSku(string $sku): void
    {
        $this->sku = $sku;
    }

    /**
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name): void
    {
        $this->name = $name;
    }

    /**
     * @return float
     */
    public function getPrice(): float
    {
        return $this->price;
    }

    /**
     * @param float $price
     */
    public function setPrice(float $price): void
    {
        $this->price = $price;
    }

    /**
     * @return string
     */
    public function getType(): string
    {
        return $this->type;
    }

    /**
     * @param string $type
     */
    public function setType(string $type): void
    {
        $this->type = $type;
    }
}