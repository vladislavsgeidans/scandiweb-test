<?php
/**
 * Class Book
 *
 * @package App\Model
 */
class Book extends BaseProduct
{
    /** @var float  */
    protected float $weight;

    /**
     * Book constructor.
     *
     * @param array $productParams
     */
    public function __construct(array $productParams)
    {
        parent::__construct(
            $productParams['id'],
            $productParams['sku'],
            $productParams['name'],
            $productParams['price'],
            $productParams['type']
        );

        $this->weight = $productParams['weight'];
    }

    /**
     * @return float
     */
    public function getWeight(): float
    {
        return $this->weight;
    }

    /**
     * @param float $weight
     */
    public function setWeight(float $weight): void
    {
        $this->weight = $weight;
    }
}