<?php
    /**
     * Class Database
     */
    class Database {
        const DATABASE_HOST = 'localhost';
        const DATABASE_NAME = 'scandiweb_trial';
        const DATABASE_USER = 'root';
        const DATABASE_PASSWORD = '';

        private static $instance = NULL;

        /**
         * @return false|\mysqli|null
         */
        public static function getInstance()
        {
            if (!isset(self::$instance)) {
                self::$instance = mysqli_connect(
                    self::DATABASE_HOST,
                    self::DATABASE_USER,
                    self::DATABASE_PASSWORD,
                    self::DATABASE_NAME
                );
            }

            return self::$instance;
        }
    }

?>