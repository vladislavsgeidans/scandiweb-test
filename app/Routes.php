<?php
    /**
     * Class Routes
     */
    class Routes
    {
        /** All site routes */
        const ROUTES = [
            "product" => ['all', 'showAll', 'add', 'delete', 'checkSku']
        ];

        /**
         * Routes constructor.
         *
         * @param string $controller
         * @param string $action
         */
        public function __construct(
            string $controller,
            string $action
        )
        {
            if (array_key_exists($controller, self::ROUTES)) {
                if (in_array($action, self::ROUTES[$controller])) {
                    $route = $this->call($controller, $action);
                } else {
                    $route = $this->call('product', 'error');
                }
            } else {
                $route = $this->call('product', 'error');
            }

            return $route;
        }

        /**
         * @param string $controller
         * @param string $action
         *
         * @return mixed
         */
        protected function call(string $controller, string $action)
        {
            $controller = new $controller;
            return $controller->{$action}();
        }
    }