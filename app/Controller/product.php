<?php
/**
 * Class product
 */
class product
{
    /** @var array  */
    const PRODUCT_TYPES_CONTROLLERS = [
        'dvd' => 'DvdDisc',
        'book' => 'Book',
        'furniture' => 'Furniture'
    ];

    /**
     * Return index page with products
     */
    function all()
    {
        $siteTitle = "All products - ScandiWeb Test task";
        $productsArray = [];
        $productsResult = mysqli_query(Database::getInstance(),
            "SELECT * FROM products ORDER BY id DESC");

        while ($productRow = mysqli_fetch_assoc($productsResult))
        {
            $productClass = self::PRODUCT_TYPES_CONTROLLERS[$productRow['type']];
            $product = new $productClass($productRow);

            $productsArray[] = $product;
        }

        require_once("./app/View/product/index.php");
    }

    /**
     * Create new product or show form
     */
    function add()
    {
        if (isset($_POST['sku']) && $_POST['sku'] !== '' && $_POST['sku'] !== null) {
            $sku = stripcslashes(trim($_POST['sku']));
            $name = stripcslashes(trim($_POST["name"]));
            $price = floatval($_POST['price']);
            $type = stripcslashes(trim($_POST['productType']));
            $size = intval($_POST['size']);
            $weight = floatval($_POST['weight']);
            $dimensions = intval($_POST['height']) . 'x' . intval($_POST['width']) . 'x' . intval($_POST['length']);

            if (BaseProduct::save($sku, $name, $price, $type, $size, $weight, $dimensions))
            {
                echo 'true';
            }
        } else {
            $siteTitle = "Create product - ScandiWeb Test task";

            require_once("./app/View/product/form.php");
        }
    }

    /**
     * Delete products
     */
    function delete()
    {
        if (isset($_POST['ids']) && count($_POST['ids']) > 0) {
            $ids = [];
            foreach ($_POST['ids'] as $id) {
                $ids[] = (int) $id;
            }
            $ids = implode(',', $ids);

            $result = mysqli_query(Database::getInstance(),
                "DELETE FROM products WHERE id IN ({$ids})"
            );

            if ($result) {
                echo 'true';
            }
        }
    }

    /**
     * Check if SKU already exists
     */
    function checkSku()
    {
        if (isset($_POST['sku']) && $_POST['sku'] !== '' && $_POST['sku'] !== null) {
            $result = mysqli_query(Database::getInstance(),
                "SELECT * FROM products WHERE sku = '" . stripslashes(trim($_POST['sku'])) . "'"
            );

            echo (mysqli_num_rows($result) > 0) ? 'false' : 'true';
        } else {
            echo 'false';
        }
    }

    /**
     * if exists error on controller/action call
     */
    function error()
    {
        $siteTitle = "All products - ScandiWeb Test task";
        require_once("./app/View/product/index.php");
    }
}