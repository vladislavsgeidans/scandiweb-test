<?php
    require_once './app/Routes.php';
    require_once './app/Database.php';
    require_once './app/Model/BaseProduct.php';
    require_once './app/Model/Book.php';
    require_once './app/Model/DvdDisc.php';
    require_once './app/Model/Furniture.php';
    require_once './app/Controller/product.php';

    if (isset($_GET["controller"]) && isset($_GET["action"])) {
        $controller = $_GET["controller"];
        $action = $_GET["action"];
    } else {
        $controller="product";
        $action="all";
    }

    $route = new Routes($controller, $action);

