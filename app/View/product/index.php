<?php include "./app/View/header.php" ?>

<div class="row p-4">
    <div class="col d-flex justify-content-start">
        <h3>Product list</h3>
    </div>
    <div class="col d-flex justify-content-end">
        <div>
            <a href="/?controller=product&action=add" class="btn btn-outline-success">ADD</a>
            <a href="#" id="delete-product-btn" class="btn btn-outline-danger">MASS DELETE</a>
        </div>
    </div>
</div>
<div class="row p-4">
    <div class="col-md-12 d-flex flex-row flex-wrap justify-content-start">
        <?php
            foreach ($productsArray as $product) {
        ?>
            <div class="row card m-1" data-id="<?= $product->getId(); ?>">
                <div class="card-body">
                    <div class="col d-flex flex-column">
                        <div class="row py-2">
                            <div class="card-title d-flex flex-row align-items-center">
                                <input type="checkbox" data-id="<?= $product->getId(); ?>" class="delete-checkbox" />
                            </div>
                        </div>
                        <div class="row">
                            <p class="fw-bold py-1">SKU: <?= $product->getSku(); ?></p>
                            <h6 class="card-subtitle mb-2">Name: <?= $product->getName(); ?></h6>
                            <p class="card-text">
                                <?php
                                    $productType = $product->getType();

                                    if ($productType === 'dvd') { ?>
                                        <span>Size: <?= $product->getSize(); ?>MB</span>
                                    <?php } else if ($productType === 'book') { ?>
                                        <span>Weight: <?= $product->getWeight(); ?>KG</span>
                                    <?php } else if ($productType === 'furniture') { ?>
                                        <span>Dimension: <?= $product->getDimensions(); ?></span>
                                    <?php }
                                ?>
                            </p>
                        </div>
                        <div class="row py-2 price-block">
                            <span class="fw-bold">
                                <?= number_format($product->getPrice(), '2', '.', ''); ?> $
                            </span>
                        </div>
                    </div>
                </div>
            </div>
        <?php
            }
        ?>
    </div>
</div>

<?php include "./app/View/footer.php" ?>
