<?php include "./app/View/header.php"; ?>

<form id="product_form">
    <div class="row p-4">
        <div class="col d-flex justify-content-start">
            <h3>Add Product</h3>
        </div>
        <div class="col d-flex justify-content-end">
            <div>
                <input type="submit" name="addSubmit" class="btn btn-success" value="Save">
                <a href="/" class="btn btn-outline-secondary">Cancel</a>
            </div>
        </div>
    </div>

    <div class="row p-4">
        <div class="col">
            <div class="form-group py-1">
                <label for="sku" class="py-1">SKUs <text class="text-red">*</text></label>
                <input type="text" class="form-control" id="sku" name="sku">
                <span class="hide fw-bold text-red error-label" data-type="sku"></span>
            </div>
            <div class="form-group py-1">
                <label for="name" class="py-1">Name <text class="text-red">*</text></label>
                <input type="text" class="form-control" id="name" name="name">
                <span class="hide fw-bold text-red error-label" data-type="name"></span>
            </div>
            <div class="form-group py-1">
                <label for="price" class="py-1">Price <text class="text-red">*</text></label>
                <input type="text" class="form-control" id="price" name="price">
                <span class="hide fw-bold text-red error-label" data-type="price"></span>
            </div>
            <div class="form-group py-1">
                <label for="productType" class="py-1">Type Switcher <text class="text-red">*</text></label>
                <select class="productType-switcher form-select" id="productType" name="productType">
                    <option disabled selected>Select type</option>
                    <option value="dvd">DVD</option>
                    <option value="book">Book</option>
                    <option value="furniture">Furniture</option>
                </select>
                <span class="hide fw-bold text-red error-label" data-type="productType"></span>
            </div>
            <br/>
            <div class="switcher-block">
                <div class="dvd-block additional_block hide">
                    <div class="form-group py-2">
                        <label for="size" class="py-1">Size <text class="text-red">*</text></label>
                        <input type="text" class="form-control" id="size" name="size">
                        <span class="hide fw-bold text-red error-label" data-type="size"></span>
                        <p class="small text-red">Please, provide size</p>
                    </div>
                </div>
                <div class="book-block additional_block hide">
                    <div class="form-group py-2">
                        <label for="weight" class="py-1">Weight <text class="text-red">*</text></label>
                        <input type="text" class="form-control" id="weight" name="weight">
                        <span class="hide fw-bold text-red error-label" data-type="weight"></span>
                        <p class="small text-red">Please, provide weight</p>
                    </div>
                </div>
                <div class="furniture-block additional_block hide">
                    <div class="form-group py-2">
                        <label for="height" class="py-1">Height <text class="text-red">*</text></label>
                        <input type="text" class="form-control" id="height" name="height">
                        <span class="hide fw-bold text-red error-label" data-type="height"></span>
                    </div>
                    <div class="form-group py-2">
                        <label for="width" class="py-1">Width <text class="text-red">*</text></label>
                        <input type="text" class="form-control" id="width" name="width">
                        <span class="hide fw-bold text-red error-label" data-type="width"></span>
                    </div>
                    <div class="form-group py-2">
                        <label for="length" class="py-1">Length <text class="text-red">*</text></label>
                        <input type="text" class="form-control" id="length" name="length">
                        <span class="hide fw-bold text-red error-label" data-type="length"></span>
                    </div>
                    <p class="small px-1 py-2 text-red">Please, provide dimensions</p>
                </div>
            </div>
        </div>
    </div>
</form>

<?php  include "./app/View/footer.php"; ?>