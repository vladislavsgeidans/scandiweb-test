$(document).ready(function() {

    //Check items on main page
    $('.card').on('click', function () {
        if ($(this).hasClass('selected')) {
            $(this).removeClass('selected');
            $('.delete-checkbox[data-id=' + $(this).data('id') + ']').prop("checked", false);
        } else {
            $(this).addClass('selected');
            $('.delete-checkbox[data-id=' + $(this).data('id') + ']').prop("checked", true);
        }
    });

    //Mass delete button logic
    $('#delete-product-btn').on('click', function(e) {
        e.preventDefault();

        let productsIds = [];

        $('.delete-checkbox:checked').each(function() {
            productsIds.push($(this).data('id'));
        });

        if (productsIds.length > 0) {
            $.ajax({
                type: "POST",
                url: "/?controller=product&action=delete",
                data: {
                    ids: productsIds
                },
                success: function(response) {
                    if (response === 'true') {
                        location.href = '/?controller=product&action=all';
                    }
                }
            });
        }
    });

    // Switcher on form
    $('.productType-switcher').on('change', function () {
        $('.additional_block').each(function () {
            $(this).addClass('hide');
        });
        $('.' + $(this).val() + '-block').removeClass('hide');
    });

    //product form submitting
    $('#product_form').on('submit', function(e) {
        e.preventDefault();

        let formData = {
            'sku': $('#sku').val(),
            'name': $('#name').val(),
            'price': $('#price').val(),
            'productType': $('#productType').val(),
            'size': $('#size').val(),
            'weight': $('#weight').val(),
            'height': $('#height').val(),
            'width': $('#width').val(),
            'length': $('#length').val()
        };

        if (formValidate(formData)) {
            $.ajax({
                type: "POST",
                async: false,
                dataType: 'text',
                url: "/?controller=product&action=add",
                data: formData,
                success: function(response) {
                    if (response === 'true') {
                        location.href = '/?controller=product&action=all';
                    }
               }
            });
        }
    });
});

//form validation method. return true\false
function formValidate(formData) {
    let validationResult = {};
    const errorValidationResult = {
        'status': 'error',
        'message': 'Please, submit required data'
    };
    const successValidationResult = {
        'status': 'ok',
        'message': ''
    };

    $.each(formData, function(key, value) {
        switch (key)
        {
            // validation string value
            case "sku":
                if (value !== '' && value !== null) {
                    $.ajax({
                        type: "POST",
                        dataType: 'text',
                        async: false,
                        url: "/?controller=product&action=checkSku",
                        data: {
                            'sku': value
                        },
                        success: function(response) {
                            if (response === 'false') {
                                validationResult[key] = {
                                    'status': 'error',
                                    'message': 'A product with such a SKU already exists'
                                };
                            } else {
                                validationResult[key] = successValidationResult;
                            }
                        }
                    });
                } else {
                    validationResult[key] = errorValidationResult;
                }

                break;
            case "name":
            case "productType":
                validationResult[key] = (value === '' || value === null) ? errorValidationResult : successValidationResult;

                break;
            // validation int value
            case "price":
                validationResult[key] = (value.length > 0 && $.isNumeric(value)) ? successValidationResult : errorValidationResult;

                break;

            // validation specific attributes
            case "size":
                if (formData.productType === 'dvd') {
                    validationResult[key] = (value.length > 0 && $.isNumeric(value)) ? successValidationResult : errorValidationResult;
                }

                break;
            case "weight":
                if (formData.productType === 'book') {
                    validationResult[key] = (value.length > 0 && $.isNumeric(value)) ? successValidationResult : errorValidationResult;
                }

                break;
            case "width":
            case "height":
            case "length":
                if (formData.productType === 'furniture') {
                    validationResult[key] = (value.length > 0 && $.isNumeric(value)) ? successValidationResult : errorValidationResult;
                }

                break;

        }
    });

    let result = true;
    $.each(validationResult, function (key, value) {
        let inputElement = $('#' + key);
        let inputLabelElement = $('.error-label[data-type=' + key + ']');

        if (value.status === 'error') {
            inputElement.addClass('error');
            inputLabelElement.removeClass('hide');
            inputLabelElement.html(value.message);

            result = false;

            return false;
        } else if (value.status === 'ok') {
            inputElement.removeClass('error');
            inputLabelElement.addClass('hide');
            inputLabelElement.html('');
        }
    });

    return result;
}